Shader "Unlit/Shader1"
{
    Properties //variables

    {
        _ColorA ("Color A", Color) = (1, 1, 1, 1)
        _ColorB ("Color B", Color) = (1, 1, 1, 1)

        // _Scale ("UV Scale", Float) = 1
        // _Offset ("UV _Offset", Float) = 1

        _ColorStart ("Color Start", Range(0, 1)) = 0
        _ColorEnd ("Color End", Range(0, 1)) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            float4 _ColorA;
            float4 _ColorB;

            // float _Scale;
            // float _Offset;

            float _ColorStart;
            float _ColorEnd;

            //automatically filled by unity
            struct MeshData //per-vertex mesh data => input vertex shader

            {
                float4 vertex : POSITION; //vertex position
                float3 normals : NORMAL;
                float4 uv0 : TEXCOORD0; // uv diffuse/normal coordinate texture
                // float4 uv1 : TEXCOORD1; // uv lightmap coordinate texture

            };

            struct Interpolators
            {
                float4 vertex : SV_POSITION;  // clip space position => output vertex shader
                float3 normal : NORMAL;
                float4 uv0 : TEXCOORD0;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            Interpolators vert(MeshData v)
            {
                Interpolators o;
                o.vertex = UnityObjectToClipPos(v.vertex); //local space to clip space
                // o.normal = v.normals;
                o.normal = UnityObjectToWorldNormal(v.normals);
                // o.uv = (v.uv0 + _Offset) * _Scale;
                o.uv0 = v.uv0;
                // o.uv = (v.uv0 + _Offset);
                // o.normal = normalize(mul((float3x3)UNITY_MATRIX_M, v.normals));
                // o.normal = normalize(mul((float3x3)unity_WorldToObject, v.normals));
                // o.normal = normalize(mul(v.normals, (float3x3)unity_WorldToObject));
                return o;
            }

            float InverseLerp(float a, float b, float v)
            {
                return (v - a) / (b - a);
            }

            float4 frag(Interpolators i) : SV_Target //=> semantic that indicates a shader outputs a color in fragment shaders

            {
                // //lerp
                // // float4 outColor = lerp(_ColorA, _ColorB, i.uv.x);
                // float t = InverseLerp(_ColorStart, _ColorEnd, i.uv0.x);
                // // float t = saturate(InverseLerp(_ColorStart, _ColorEnd, i.uv.x));
                // // t = saturate(t);
                // t = frac(t);
                // return t;
                // // float t = InverseLerp(_ColorStart, _ColorEnd, i.uv.x) - 1;
                // // float t = 1 - InverseLerp(_ColorStart, _ColorEnd, i.uv.x);
                // float4 outColor = lerp(_ColorA, _ColorB, t);
                // return outColor;
                // // return t;

                return float4(i.normal, 0);
                // return float4(i.uv.x, i.uv.y, 0, 1);
                // return float4(UnityObjectToWorldNormal(i.normal), 0);

            }
            ENDCG
        }
    }
}
