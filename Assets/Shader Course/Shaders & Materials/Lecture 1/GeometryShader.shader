Shader "Unlit/Shader1"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            
            #pragma geometry vert

            struct appdata{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };
             
            struct v2g{
                float4 objPos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };
             
            struct g2f{
                float4 worldPos : SV_POSITION;
                float2 uv : TEXCOORD0;
                fixed4 col : COLOR;
            };

            [maxvertexcount(12)]
            void geom (triangle v2g input[3], inout TriangleStream<g2f> tristream){
                // here goes the real logic.
            }

            ENDCG
        }
    }
}
